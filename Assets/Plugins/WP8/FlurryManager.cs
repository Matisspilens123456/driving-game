﻿//#if UNITY_WP8

//using FlurryWP8SDK.Models;
//using SBS.Flurry;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace HotRod.Classes
//{
//    public static class FlurryManager
//    {
//        const string SBS_API_KEY = "";
//        const string MINICLIP_API_KEY = "";

//        const string FLURRY_API_KEY = MINICLIP_API_KEY;

//        public static void OnApplicationLauched()
//        {
//            FlurryWP8SDK.Api.StartSession(FLURRY_API_KEY);
//        }

//        public static void OnApplicationActivated()
//        {
//            FlurryWP8SDK.Api.StartSession(FLURRY_API_KEY);
//        }

//        public static void SubcribeToUnityEvents()
//        {
//            FlurryBinding.LogEvent_WP8 += (args) => { OnLogEvent(args); };
//            FlurryBinding.EndTimedEvent_WP8 += (args) => { OnEndTimedEvent(args); };
//        }

//        private static void OnLogEvent(EventArgs args)
//        {
//            FlurryBinding.LogEventArgs eventArgs = args as FlurryBinding.LogEventArgs;
//            if (eventArgs == null)
//                return;

//            string eventName = eventArgs.EventName;
//            List<Parameter> eventParameters = null;
//            if (eventArgs.EventParams != null)
//            {
//                eventParameters = new List<Parameter>();

//                foreach (var kvp in eventArgs.EventParams)
//                    eventParameters.Add(new Parameter(kvp.Key, kvp.Value));
//            }
//            bool timed = eventArgs.Timed;

//            if (eventParameters == null)
//                FlurryWP8SDK.Api.LogEvent(eventName, timed);
//            else
//                FlurryWP8SDK.Api.LogEvent(eventName, eventParameters, timed);
//        }

//        private static void OnEndTimedEvent(EventArgs args)
//        {
//            FlurryBinding.EndTimedEventArgs eventArgs = args as FlurryBinding.EndTimedEventArgs;
//            if (eventArgs == null)
//                return;

//            string eventName = eventArgs.EventName;
//            List<Parameter> newParameters = null;
//            if (eventArgs.NewParams != null)
//            {
//                newParameters = new List<Parameter>();

//                foreach (var kvp in eventArgs.NewParams)
//                    newParameters.Add(new Parameter(kvp.Key, kvp.Value));
//            }

//            if (newParameters == null)
//                FlurryWP8SDK.Api.EndTimedEvent(eventName);
//            else
//                FlurryWP8SDK.Api.EndTimedEvent(eventName, newParameters);
//        }
//    }
//}

//#endif