﻿//#define USE_MOCK_INAPP

//#if UNITY_WP8

//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using SBS.Miniclip;

//#if USE_MOCK_INAPP
//using MockIAPLib;
//using Store = MockIAPLib;
//#else
//using Windows.ApplicationModel.Store;
//using Store = Windows.ApplicationModel.Store;
//#endif

//namespace RailRush.Classes
//{
//    public static class InAppPurchaseManager
//    {
        
//        public static void SubcribeToUnityEvents()
//        {
//            WP8InappBindings.LoadStoreEvent += (args) => { System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() => { OnLoadStoreRequest(); }); };
//            WP8InappBindings.PurchaseProductEvent += (args) => { System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() => { OnPurchaseProductRequest(args); }); };
//            WP8InappBindings.RestorePurchasesEvent += (args) => { System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() => { OnRestorePurchasesRequest(); }); };
//        }
        
//        private static async void OnLoadStoreRequest()
//        {
//            try 
//            {
//                ListingInformation li = await Store.CurrentApp.LoadListingInformationAsync();
        
//                int productsCount = li.ProductListings.Count; 
//                //System.Diagnostics.Debug.WriteLine("### ### ### OnLoadStoreRequest() - productsCount: " + productsCount);

//                HotRodInAppManager.inAppPurchProduct[] products = new HotRodInAppManager.inAppPurchProduct[productsCount];
//                int i = 0;
        
//                foreach (KeyValuePair<string, ProductListing> kvp in li.ProductListings)
//                {
//                    ProductListing pListing = kvp.Value;
        
//                    // To Restore Purchases
//                    /*
//                    bool isOwned = false;
//                    if (Store.CurrentApp.LicenseInformation.ProductLicenses.ContainsKey(kvp.Key))
//                        isOwned = Store.CurrentApp.LicenseInformation.ProductLicenses[kvp.Key].IsActive;
//                    if (isOwned)
//                    {
//                        System.Diagnostics.Debug.WriteLine("### ### ### OnLoadStoreRequest() - product: " + kvp.Key + " is owned.");
//                        continue;
//                    }
//                    */
        
//                    string productId = pListing.ProductId;
//                    string title = pListing.Name;
//                    string description = "";
//                    string price = pListing.FormattedPrice;
        
//                    //System.Diagnostics.Debug.WriteLine("### ### ### OnLoadStoreRequest() - PRODUCT:\nid: " + productId + "\ntitle: " + title + "\ndescription: " + description + "\nprice: " + price);
        
//                    products[i] = new HotRodInAppManager.inAppPurchProduct();
//                    products[i].id = productId;
//                    products[i].title = title;
//                    products[i].description = description;
//                    products[i].price = price;
//                    i++;
//                }
        
//                WP8InappBindings.OnStoreLoaded(products);
//            }
//            catch (Exception e)
//            {
//                System.Diagnostics.Debug.WriteLine("### ### ### OnLoadStoreRequest() - EXCEPTION:\n" + e.ToString());
//                return;
//            }
//        }
        
//        /*
//        static HotRodInAppManager.inAppPurchProduct[] getTestProducts()
//        {
//            List<HotRodInAppManager.inAppPurchProduct> products = new List<HotRodInAppManager.inAppPurchProduct>();

//            HotRodInAppManager.inAppPurchProduct prod = null;
//            prod = new HotRodInAppManager.inAppPurchProduct();
//            prod.id = "purchase_money_0";
//            prod.title = "title";
//            prod.description = "description";
//            prod.price = "10 $";
//            products.Add(prod);
        
//            prod = new HotRodInAppManager.inAppPurchProduct();
//            prod.id = "purchase_money_1";
//            prod.title = "title";
//            prod.description = "description";
//            prod.price = "20 $";
//            products.Add(prod);

//            prod = new HotRodInAppManager.inAppPurchProduct();
//            prod.id = "purchase_money_2";
//            prod.title = "title";
//            prod.description = "description";
//            prod.price = "30 $";
//            products.Add(prod);
        
//            prod = new HotRodInAppManager.inAppPurchProduct();
//            prod.id = "purchase_money_3";
//            prod.title = "title";
//            prod.description = "description";
//            prod.price = "40 $";
//            products.Add(prod);
        
//            prod = new HotRodInAppManager.inAppPurchProduct();
//            prod.id = "purchase_money_4";
//            prod.title = "title";
//            prod.description = "description";
//            prod.price = "50 $";
//            products.Add(prod);
        
//            prod = new HotRodInAppManager.inAppPurchProduct();
//            prod.id = "purchase_crowns_0";
//            prod.title = "title";
//            prod.description = "description";
//            prod.price = "110 $";
//            products.Add(prod);
        
//            prod = new HotRodInAppManager.inAppPurchProduct();
//            prod.id = "purchase_crowns_1";
//            prod.title = "title";
//            prod.description = "description";
//            prod.price = "120 $";
//            products.Add(prod);
        
//            prod = new HotRodInAppManager.inAppPurchProduct();
//            prod.id = "purchase_crowns_2";
//            prod.title = "title";
//            prod.description = "description";
//            prod.price = "130 $";
//            products.Add(prod);
        
//            prod = new HotRodInAppManager.inAppPurchProduct();
//            prod.id = "purchase_crowns_3";
//            prod.title = "title";
//            prod.description = "description";
//            prod.price = "140 $";
//            products.Add(prod);
        
//            prod = new HotRodInAppManager.inAppPurchProduct();
//            prod.id = "purchase_crowns_4";
//            prod.title = "title";
//            prod.description = "description";
//            prod.price = "150 $";
//            products.Add(prod);
        
//            prod = new HotRodInAppManager.inAppPurchProduct();
//            prod.id = "purchase_crowns_5";
//            prod.title = "title";
//            prod.description = "description";
//            prod.price = "160 $";
//            products.Add(prod);
        
//            return products.ToArray();
//        }
//        */

//        private static async void OnPurchaseProductRequest(EventArgs args)
//        {
//            try
//            {
//                string identifier = (args as WP8InappBindings.ProductPurchaseArgs).Identifier;
        
//                /*string receipt = */ await Store.CurrentApp.RequestProductPurchaseAsync(identifier, false);
        
//                ProductLicense productLicense = null;
//                if (CurrentApp.LicenseInformation.ProductLicenses.TryGetValue(identifier, out productLicense))
//                {
//                    if (productLicense.IsActive)
//                    {
//                        if (productLicense.IsConsumable)
//                        {
//                            CurrentApp.ReportProductFulfillment(identifier);
//                        }
        
//                        WP8InappBindings.OnProductPurchased(identifier);
//                        return;
//                    }
//                }
        
//                WP8InappBindings.OnTransactionFailed();
//            }
//            catch (Exception e)
//            {
//                System.Diagnostics.Debug.WriteLine("### ### ### OnPurchaseProductRequest() - EXCEPTION:\n" + e.ToString());
//                WP8InappBindings.OnTransactionFailed();
//                return;
//            }
//        }
        
//        private static async void OnRestorePurchasesRequest()
//        {
//            try
//            {
//                ListingInformation li = await Store.CurrentApp.LoadListingInformationAsync();
        
//                int productsCount = li.ProductListings.Count;
//                //System.Diagnostics.Debug.WriteLine("### ### ### OnRestorePurchasesRequest() - productsCount: " + productsCount);
                
//                HotRodInAppManager.inAppPurchProduct[] products = new HotRodInAppManager.inAppPurchProduct[productsCount];
                
//                foreach(KeyValuePair<string, ProductListing> kvp in li.ProductListings)
//                {
//                    ProductListing pListing = kvp.Value;
        
//                    if (pListing.ProductType == Windows.ApplicationModel.Store.ProductType.Consumable)
//                    {
//                        //System.Diagnostics.Debug.WriteLine("### ### ### OnRestorePurchasesRequest() - Skipping consumable product: " + pListing.ProductId);
//                        continue;
//                    }
        
//                    bool isOwned = false;
//                    if (Store.CurrentApp.LicenseInformation.ProductLicenses.ContainsKey(kvp.Key))
//                        isOwned = Store.CurrentApp.LicenseInformation.ProductLicenses[kvp.Key].IsActive;
        
//                    //System.Diagnostics.Debug.WriteLine("### ### ### OnRestorePurchasesRequest() - product: " + pListing.ProductId + ", isOwned: " + isOwned);
                    
//                    if (isOwned)
//                        WP8InappBindings.OnPurchaseRestored(pListing.ProductId);
//                }
//            }
//            catch
//            {
//                return;
//            }
//        }
//    }
//}

//#endif