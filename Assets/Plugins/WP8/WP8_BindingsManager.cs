﻿//#if UNITY_WP8
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using Microsoft.Phone.Controls;
//using Microsoft.Phone.Tasks;
//using SBS.Miniclip;
//using System.Threading.Tasks;
//using System.Windows;

//namespace OnTheRun.Classes
//{
//    class WP8_BindingsManager
//    {
//        public static void SubcribeToUnityEvents()
//        {
//            WP8Bindings.MoreGamesEvent += (args) => { OnMoreGamesEvent(args); };
//            WP8Bindings.RateItEvent += (args) => { OnRateItEvent(args); };
//            WP8Bindings.ExitEvent += (args) => { OnExitEvent(args); };
//        }

//        private static void OnMoreGamesEvent(EventArgs args)
//        {
//            WP8Bindings.MoreGamesArgs eventArgs = args as WP8Bindings.MoreGamesArgs;
//            if (eventArgs == null)
//                return;

//            WebBrowserTask webBrowserTask = new WebBrowserTask();
//            webBrowserTask.Uri = new Uri(eventArgs.Url, UriKind.Absolute);
//            webBrowserTask.Show();
//        }

//        private static void OnRateItEvent(EventArgs args)
//        {
//            WP8Bindings.RateItEventArgs eventArgs = args as WP8Bindings.RateItEventArgs;
//            if (eventArgs == null)
//                return;

//            System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
//            {
//                MessageBoxResult result;

//                result = MessageBox.Show(eventArgs.Text, eventArgs.Title, MessageBoxButton.OKCancel);

//                if (result == MessageBoxResult.OK)
//                {
//                    WebBrowserTask webBrowserTask = new WebBrowserTask();
//                    webBrowserTask.Uri = new Uri(eventArgs.Url, UriKind.Absolute);
//                    webBrowserTask.Show();
//                }
//            });
//        }

//        private static void OnExitEvent(EventArgs args)
//        {
//            WP8Bindings.ExitEventArgs eventArgs = args as WP8Bindings.ExitEventArgs;
//            if (eventArgs == null)
//                return;

//            System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
//            {
//                MessageBoxResult result;

//                result = MessageBox.Show(eventArgs.Text, eventArgs.Title, MessageBoxButton.OKCancel);

//                if (result == MessageBoxResult.OK)
//                {
//                    EncryptedPlayerPrefs.Save();
//                    UnityEngine.Application.Quit();
//                }
//            });
//        }
//    }
//}
//#endif